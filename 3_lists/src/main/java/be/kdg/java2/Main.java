package be.kdg.java2;

import be.kdg.java2.kollections.ArrayList;
import be.kdg.java2.kollections.Kollections;
import be.kdg.java2.kollections.LinkedList;
import be.kdg.java2.kollections.List;

import java.util.Random;


public class Main {

    public static void main(String[] args) {
        PerformanceTester.compareArrayListAndLinkedList(100000);
        List<Student> myArrayList = new ArrayList<>();
        myArrayList.add(new Student(3, "jos"));
        myArrayList.add(new Student(5, "marie"));
        myArrayList.add(new Student(2, "jef"));
        myArrayList.add(new Student(7, "dirk"));
        for (int i = 0; i < 1000; i++) {
            myArrayList.add(new Student(new Random().nextInt(1000), "student" + i));
        }
        Kollections.quickSort(myArrayList);
        System.out.println("After sort:");
        for (int i=0;i<myArrayList.size();i++){
            System.out.println(myArrayList.get(i));
        }
        Student studentToFind = new Student(5, "marie");
        System.out.println("Index of student " + studentToFind + ": " + Kollections.lineairSearch(myArrayList, studentToFind));
        System.out.println("Index of student " + studentToFind + ": " +  Kollections.binarySearch(myArrayList, studentToFind));
        System.out.println("==========SELECTIONSORT================");
        PerformanceTester.testSelectionSort();
        System.out.println("==========MERGESORT====================");
        PerformanceTester.testMergeSort();
    }
}
